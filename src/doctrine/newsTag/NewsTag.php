<?php

declare(strict_types=1);

namespace Skadmin\News\Doctrine\News;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class NewsTag
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column]
    private string $color = '';

    /** @var ArrayCollection|Collection|News[] */
    #[ORM\ManyToMany(targetEntity: News::class, mappedBy: 'tags')]
    #[ORM\OrderBy(['validityFrom' => 'ASC'])]
    private ArrayCollection|Collection|array $news;

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }

    public function update(string $name, string $content, string $color, bool $isActive): void
    {
        $this->name    = $name;
        $this->content = $content;
        $this->color   = $color;

        $this->setIsActive($isActive);
    }

    public function getColor(): string
    {
        return $this->color === '' ? '#FFFFFF' : $this->color;
    }

    /**
     * @return ArrayCollection|Collection|News[]
     */
    public function getNews(): ArrayCollection|Collection|array
    {
        return $this->news;
    }
}
