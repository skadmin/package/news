<?php

declare(strict_types=1);

namespace Skadmin\News\Doctrine\News;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class NewsTagFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = NewsTag::class;
    }

    public function create(string $name, string $content, string $color, bool $isActive): NewsTag
    {
        return $this->update(null, $name, $content, $color, $isActive);
    }

    public function update(?int $id, string $name, string $content, string $color, bool $isActive): NewsTag
    {
        $newsTag = $this->get($id);
        $newsTag->update($name, $content, $color, $isActive);

        if (! $newsTag->isLoaded()) {
            $newsTag->setWebalize($this->getValidWebalize($name));
            $newsTag->setSequence($this->getValidSequence());
        }

        $this->em->persist($newsTag);
        $this->em->flush();

        return $newsTag;
    }

    public function get(?int $id = null): NewsTag
    {
        if ($id === null) {
            return new NewsTag();
        }

        $newsTag = parent::get($id);

        if ($newsTag === null) {
            return new NewsTag();
        }

        return $newsTag;
    }

    /**
     * @return NewsTag[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?NewsTag
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
