<?php

declare(strict_types=1);

namespace Skadmin\News\Doctrine\News;

use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\News\Events\EventNewsCreate;
use Skadmin\News\Events\EventNewsUpdate;
use SkadminUtils\DoctrineTraits\Facade;

use SkadminUtils\News\Factory\NewsSettingsFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use function assert;
use function intval;

final class NewsFacade extends Facade
{
    use Facade\Webalize;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EntityManagerDecorator $em, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($em);

        $this->eventDispatcher = $eventDispatcher;

        $this->table = News::class;
    }

    public function getModelForGrid(): QueryBuilder
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        return $repository->createQueryBuilder('a')
            ->leftJoin('a.tags', 't');
    }

    /**
     * @param NewsTag[]|array $tags
     */
    public function create(string $name, string $content, string $description, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, ?string $imagePreview, string $lastUpdateAuthor, bool $isLocked, bool $isImportant, array $tags): News
    {
        $news = $this->update(null, $name, $content, $description, $isActive, $validityFrom, $validityTo, $imagePreview, $lastUpdateAuthor, $isLocked, $isImportant, $tags);
        $this->eventDispatcher->dispatch(new EventNewsCreate($news));
        return $news;
    }

    /**
     * @param NewsTag[]|array $tags
     */
    public function update(?int $id, string $name, string $content, string $description, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, ?string $imagePreview, string $lastUpdateAuthor, bool $isLocked, bool $isImportant, array $tags): News
    {
        $news = $this->get($id);
        $news->update($name, $content, $description, $isActive, $validityFrom, $validityTo, $imagePreview, $lastUpdateAuthor, $isLocked, $isImportant, $tags);

        if (! $news->isLoaded()) {
            $news->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($news);
        $this->em->flush();

        if ($id !== null) {
            $this->eventDispatcher->dispatch(new EventNewsUpdate($news));
        }

        return $news;
    }

    public function get(?int $id = null): News
    {
        if ($id === null) {
            return new News();
        }

        $news = parent::get($id);

        if ($news === null) {
            return new News();
        }

        return $news;
    }

    public function getCountValidityNews(): int
    {
        $qb = $this->findValidityNewsQb();
        $qb->select('count(a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return News[]
     */
    public function findValidityNews(?int $limit = null, ?int $offset = null): array
    {
        return $this->findValidityNewsQb($limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findValidityNewsQb(?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $currentDateTimeStart = (new DateTime())->setTime(0, 0, 0);
        $currentDateTimeEnd   = (new DateTime())->setTime(23, 59, 59);

        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.validityFrom', 'DESC');

        $criteria = Criteria::create();
        $criteria
            ->where(Criteria::expr()->eq('a.isActive', true))
            ->andWhere(Criteria::expr()->orX(
                Criteria::expr()->gte('a.validityTo', $currentDateTimeStart),
                Criteria::expr()->isNull('a.validityTo')
            ))
            ->andWhere(Criteria::expr()->lte('a.validityFrom', $currentDateTimeEnd));

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function getCount(?NewsFilter $filter = null): int
    {
        $qb = $this->findNewsQb($filter);
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return News[]
     */
    public function findNews(?NewsFilter $filter = null, ?int $limit = null, ?int $offset = null): array
    {
        return $this->findNewsQb($filter, $limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findNewsQb(?NewsFilter $filter = null, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->distinct();
        assert($qb instanceof QueryBuilder);

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.validityFrom', 'DESC');

        $criteria = Criteria::create();

        if ($filter !== null) {
            $filter->modifyCriteria($criteria);
        }

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function findByWebalize(string $webalize): ?News
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
