<?php

declare(strict_types=1);

namespace Skadmin\News\Doctrine\News;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class News
{
    public const DEFAULT_LENGHT_DESCRIPTION = 500;
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Description;
    use Entity\LastUpdate;
    use Entity\Validity;
    use Entity\ImagePreview;
    use Entity\Created;
    use Entity\IsLocked;
    use Entity\IsImportant;

    /** @var ArrayCollection<NewsTag>|Collection|array<NewsTag> */
    #[ORM\ManyToMany(targetEntity: NewsTag::class, inversedBy: 'news')]
    #[ORM\JoinTable(name: 'news_tag_rel')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private ArrayCollection|Collection|array $tags;

    private ?NewsTag $firstTag = null;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @param array<NewsTag> $tags
     */
    public function update(string $name, string $content, string $description, bool $isActive, DateTimeInterface $validityFrom, ?DateTimeInterface $validityTo, ?string $imagePreview, string $lastUpdateAuthor, bool $isLocked, bool $isImportant, array $tags): void
    {
        $this->name             = $name;
        $this->content          = $content;
        $this->description      = $description;
        $this->lastUpdateAuthor = $lastUpdateAuthor;

        $this->validityFrom = $validityFrom;
        $this->validityTo   = $validityTo;

        $this->tags = $tags;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        $this->setIsActive($isActive);
        $this->setIsLocked($isLocked);
        $this->setIsImportant($isImportant);
    }

    /**
     * @return ArrayCollection<NewsTag>|Collection|array<NewsTag>
     */
    public function getTags(): ArrayCollection|Collection|array
    {
        return $this->tags;
    }

    public function getFirstTags(): ?NewsTag
    {
        if ($this->firstTag === null) {
            $firstTag       = $this->tags->first();
            $this->firstTag = $firstTag ? $firstTag : null;
        }

        return $this->firstTag;
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdate(): void
    {
        if ($this->description !== '') {
            return;
        }

        $this->description = Strings::truncate($this->content, self::DEFAULT_LENGHT_DESCRIPTION);
    }
}
