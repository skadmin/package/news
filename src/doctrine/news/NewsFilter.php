<?php

declare(strict_types=1);

namespace Skadmin\News\Doctrine\News;

use Doctrine\Common\Collections\Criteria;
use Nette\SmartObject;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\ACriteriaFilter;

use function count;
use function trim;

final class NewsFilter extends ACriteriaFilter
{
    use SmartObject;

    /** @var array<NewsTag> */
    private array  $tags         = [];
    private string $phrase       = '';
    private bool   $onlyValidity = false;

    /**
     * @param array<NewsTag> $tags
     */
    public function __construct(string $phrase, bool $onlyValidity, array $tags)
    {
        $this->phrase       = $phrase;
        $this->onlyValidity = $onlyValidity;
        $this->tags         = $tags;
    }

    public function getPhrase(): string
    {
        return trim($this->phrase);
    }

    public function setPhrase(string $phrase): self
    {
        $this->phrase = $phrase;

        return $this;
    }

    public function isOnlyValidity(): bool
    {
        return $this->onlyValidity;
    }

    public function setOnlyValidity(bool $onlyValidity): void
    {
        $this->onlyValidity = $onlyValidity;
    }

    /**
     * @return array<NewsTag>
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array<NewsTag> $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void
    {
        $expr = Criteria::expr();

        if ($this->getPhrase() !== '') {
            $criteria->andWhere($expr->orX(
                $expr->contains($this->getEntityName($alias, 'name'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'content'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'description'), $this->getPhrase()),
                $expr->contains($this->getEntityName($alias, 'lastUpdateAuthor'), $this->getPhrase())
            ));
        }

        if ($this->isOnlyValidity()) {
            $currentDateTimeStart = (new DateTime())->setTime(0, 0, 0);
            $currentDateTimeEnd   = (new DateTime())->setTime(23, 59, 59);

            $criteria->where(Criteria::expr()->eq('a.isActive', true))
                ->andWhere(Criteria::expr()->orX(
                    Criteria::expr()->gte('a.validityTo', $currentDateTimeStart),
                    Criteria::expr()->isNull('a.validityTo')
                ))
                ->andWhere(Criteria::expr()->lte('a.validityFrom', $currentDateTimeEnd));
        }

        if (count($this->getTags()) <= 0) {
            return;
        }

        $criteria->andWhere($expr->in('t.id', $this->getTags()));
    }
}
