<?php

declare(strict_types=1);

namespace Skadmin\News;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'news';
    public const DIR_IMAGE = 'news';

    public const PRIVILEGE_PUBLISH = 'publish';
    public const PRIVILEGE_LOCK    = 'lock';
    public const PRIVILEGE_TAGS    = 'tags';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-newspaper']),
            'items'   => ['overview'],
        ]);
    }
}
