<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Front;

interface IDetailFactory
{
    public function create(int $id): Detail;
}
