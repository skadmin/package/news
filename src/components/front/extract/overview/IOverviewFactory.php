<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Front;

interface IOverviewFactory
{
    public function create(): Overview;
}
