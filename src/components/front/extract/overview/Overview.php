<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\News\BaseControl;
use Skadmin\News\Doctrine\News\NewsFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\ImageStorage\ImageStorage;

class Overview extends TemplateControl
{
    use APackageControl;

    private NewsFacade   $facade;
    private ImageStorage $imageStorage;

    public function __construct(NewsFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
    }

    public function getTitle(): string
    {
        return 'news.front.overview.title';
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->imageStorage = $this->imageStorage;

        $template->activeNews = $this->facade->findValidityNews();
        $template->package    = new BaseControl();

        $template->render();
    }
}
