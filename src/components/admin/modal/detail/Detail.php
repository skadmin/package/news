<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\News\Doctrine\News\News;
use Skadmin\News\Doctrine\News\NewsFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\ImageStorage\ImageStorage;

class Detail extends TemplateControl
{
    use APackageControl;

    private NewsFacade   $facade;
    private News         $news;
    private ImageStorage $imageStorage;

    public function __construct(int $id, NewsFacade $facade, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;

        $this->news    = $this->facade->get($id);
        $this->isModal = true;
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('news.detail.title - %s', $this->news->getName());
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');
        $template->imageStorage = $this->imageStorage;

        $template->news    = $this->news;
        $template->drawBox = $this->drawBox;

        $template->render();
    }
}
