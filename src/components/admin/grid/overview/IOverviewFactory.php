<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
