<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\News\BaseControl;
use Skadmin\News\Doctrine\News\News;
use Skadmin\News\Doctrine\News\NewsFacade;
use Skadmin\News\Doctrine\News\NewsTagFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Utils\Utils\Colors\Colors;

use function sprintf;

class Overview extends GridControl
{
    use APackageControl;

    private NewsFacade    $facade;
    private NewsTagFacade $facadeNewsTag;
    private ImageStorage  $imageStorage;

    public function __construct(NewsFacade $facade, NewsTagFacade $facadeNewsTag, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade        = $facade;
        $this->facadeNewsTag = $facadeNewsTag;
        $this->imageStorage  = $imageStorage;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'news.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForGrid()
            ->orderBy('a.createdAt', 'DESC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.news.overview.name')
            ->setRenderer(function (News $news): Html {
                if ($news->getImagePreview() !== null) {
                    $img = $this->imageStorage->fromIdentifier([$news->getImagePreview(), '36x36', 'exact']);

                    $elImg = Html::el('img', ['src' => '/' . $img->createLink()]);
                }

                if ((! $news->isLocked() || $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK)) && $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $news->getId(),
                    ]);
                } else {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'detail',
                        'id'      => $news->getId(),
                    ]);
                }

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ]);

                if (isset($elImg)) {
                    $href->addHtml($elImg)
                        ->addText(' ');
                }

                if ($news->isLocked()) {
                    $href->addHtml(Html::el('i', ['class' => 'fas fa-fw fa-lock mr-1']));
                }

                $href->addText($news->getName());

                return $href;
            });
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addColumnText('tags', 'grid.news.overview.tags')
                ->setRenderer(static function (News $news): Html {
                    $tags = new Html();
                    foreach ($news->getTags() as $tag) {
                        $color = $tag->getColor() !== '' ? $tag->getColor() : '#FFFFFF';
                        $tags->addHtml(Html::el('span', [
                            'class' => 'badge mr-1 px-2 mb-1 border border-primary',
                            'style' => sprintf('background-color: %s; color: %s', $color, Colors::getContrastColor($color)),
                        ])->setText($tag->getName()));
                    }

                    return $tags;
                });
        }

        $grid->addColumnText('isActive', 'grid.news.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);
        $grid->addColumnText('lastUpdateAuthor', 'grid.news.overview.lastUpdateAuthor');
        $grid->addColumnDateTime('lastUpdateAt', 'grid.news.overview.lastUpdateAt')
            ->setFormat('d.m.Y H:i');

        // FILTER
        $grid->addFilterText('name', 'grid.news.overview.name');
        $grid->addFilterSelect('isActive', 'grid.news.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addFilterSelect('tags', 'grid.news.overview.tags', $this->facadeNewsTag->getPairs('id', 'name'), 't.id')
                ->setPrompt(Constant::PROMTP);
        }

        $grid->addFilterText('lastUpdateAuthor', 'grid.news.overview.lastUpdateAuthor');

        // ACTION
        $grid->addAction('detail', 'grid.news.overview.action.detail', 'Component:default', ['id' => 'id'])->addParameters([
            'package' => new BaseControl(),
            'render'  => 'detail',
        ])->setIcon('eye')
            ->setClass('btn btn-xs btn-default btn-outline-primary ajax');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.news.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $grid->addToolbarButton('Component:default#2', 'grid.news.overview.action.overview-tag', [
                'package' => new BaseControl(),
                'render'  => 'overview-tag',
            ])->setIcon('tags')
                ->setClass('btn btn-xs btn-outline-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.news.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // IF USER ALLOWED WRITE
        $grid->allowRowsAction('edit', function (News $news): bool {
            return ! $news->isLocked() || $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK);
        });

        // ALLOW

        return $grid;
    }
}
