<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\News\BaseControl;
use Skadmin\News\Doctrine\News\NewsTag;
use Skadmin\News\Doctrine\News\NewsTagFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewTag extends GridControl
{
    use APackageControl;
    use IsActive;

    private NewsTagFacade $facade;
    private LoaderFactory $webLoader;

    public function __construct(NewsTagFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $this->onFlashmessage('grid.news.overview-tag.name.flash.info.denide-acccess-tags', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewTag.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'news.overview-tag.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.news.overview-tag.name')
            ->setRenderer(function (NewsTag $newsTag): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-tag',
                        'id'      => $newsTag->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($newsTag->getName());

                return $name;
            });
        $grid->addColumnText('color', 'grid.news.overview-tag.color')
            ->setRenderer(static function (NewsTag $newsTag): Html {
                return Html::el('span', ['data-color-view' => 'color'])
                    ->setText($newsTag->getColor());
            })->setAlign('center');
        $this->addColumnIsActive($grid, 'news.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.news.overview-tag.name');
        $this->addFilterIsActive($grid, 'news.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.news.overview-tag.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-tag',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.news.overview-tag.action.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list-ul')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.news.overview-tag.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-tag',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // IF USER ALLOWED WRITE
//        $grid->allowRowsAction('edit', function (News $news) : bool {
//            return ! $news->isLocked() || $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK);
//        });

        // ALLOW

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.news.overview-tag.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
