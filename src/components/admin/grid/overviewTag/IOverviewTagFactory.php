<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

interface IOverviewTagFactory
{
    public function create(): OverviewTag;
}
