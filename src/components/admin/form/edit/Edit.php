<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Skadmin\News\BaseControl;
use Skadmin\News\Doctrine\News\News;
use Skadmin\News\Doctrine\News\NewsFacade;
use Skadmin\News\Doctrine\News\NewsTag;
use Skadmin\News\Doctrine\News\NewsTagFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function assert;
use function explode;
use function is_bool;
use function sprintf;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private NewsFacade    $facade;
    private NewsTagFacade $facadeNewsTag;
    private User          $user;
    private News          $news;
    private ImageStorage  $imageStorage;

    public function __construct(?int $id, NewsFacade $facade, NewsTagFacade $facadeNewsTag, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade        = $facade;
        $this->facadeNewsTag = $facadeNewsTag;
        $this->webLoader     = $webLoader;
        $this->user          = $this->loggedUser->getIdentity(); //@phpstan-ignore-line
        $this->imageStorage  = $imageStorage;

        $this->news = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK) && $this->news->isLocked()) {
            $this->onFlashmessage('form.news.edit.flash.info.locked', Flash::INFO);
            $this->processOnBack();
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->news->isLoaded()) {
            return new SimpleTranslation('news.edit.title - %s', $this->news->getName());
        }

        return 'news.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        [$validityFrom, $validityTo] = Arrays::map(explode(' - ', $values->validity), static function (string $date): DateTime {
            $date = DateTime::createFromFormat('d.m.Y', $date);

            return is_bool($date) ? new DateTime() : $date;
        });
        assert($validityFrom instanceof DateTime);
        assert($validityTo instanceof DateTime);

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if (isset($values->tags)) {
            $tags = Arrays::map($values->tags, function ($tagId): NewsTag {
                return $this->facadeNewsTag->get($tagId);
            });
        }

        if ($this->news->isLoaded()) {
            if ($identifier !== null && $this->news->getImagePreview() !== null) {
                $this->imageStorage->delete($this->news->getImagePreview());
            }

            $news = $this->facade->update(
                $this->news->getId(),
                $values->name,
                $values->content,
                $values->description,
                $values->isActive ?? $this->news->isActive(),
                $validityFrom,
                $values->onlyValidityFrom ? null : $validityTo,
                $identifier,
                $this->user->getFullName(),
                $values->isLocked ?? false,
                $values->isImportant,
                $tags ?? $this->news->getTags()->toArray()
            );
            $this->onFlashmessage('form.news.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $news = $this->facade->create(
                $values->name,
                $values->content,
                $values->description,
                $values->isActive ?? false,
                $validityFrom,
                $values->onlyValidityFrom ? null : $validityTo,
                $identifier,
                $this->user->getFullName(),
                $values->isLocked ?? false,
                $values->isImportant,
                $tags ?? []
            );
            $this->onFlashmessage('form.news.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $news->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->news = $this->news;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.news.edit.name')
            ->setRequired('form.news.edit.name.req');
        $form->addText('validity', 'form.news.edit.validity')
            ->setRequired('form.news.edit.validity.req')
            ->setHtmlAttribute('data-daterange');
        $form->addTextArea('content', 'form.news.edit.content');
        $form->addTextArea('description', 'form.news.edit.description');

        $form->addImageWithRFM('imagePreview', 'form.news.edit.image-preview');

        $form->addCheckbox('onlyValidityFrom', 'form.news.edit.onlyValidityFrom')
            ->setDefaultValue(false);

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $form->addMultiSelect('tags', 'form.news.edit.tags', $this->facadeNewsTag->getPairs('id', 'name'))
                ->setTranslator(null);
        }

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_PUBLISH)) {
            $form->addCheckbox('isActive', 'form.news.edit.is-active')
                ->setDefaultValue(true);
        }

        if ($this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_LOCK)) {
            $form->addCheckbox('isLocked', 'form.news.edit.is-locked')
                ->setDefaultValue(false);
        }

        $form->addCheckbox('isImportant', 'form.news.edit.is-important')
            ->setDefaultValue(false);

        // BUTTON
        $form->addSubmit('send', 'form.news.edit.send');
        $form->addSubmit('sendBack', 'form.news.edit.send-back');
        $form->addSubmit('back', 'form.news.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->news->isLoaded()) {
            return [];
        }

        $tagsId = Arrays::map($this->news->getTags()->toArray(), static function (NewsTag $tag): ?int {
            return $tag->getId();
        });

        if ($this->news->getValidityTo() === null) {
            $validityTo = $this->news->getValidityFrom();
        } else {
            $validityTo = $this->news->getValidityTo();
        }

        return [
            'name'             => $this->news->getName(),
            'content'          => $this->news->getContent(),
            'description'      => $this->news->getDescription(),
            'isActive'         => $this->news->isActive(),
            'isLocked'         => $this->news->isLocked(),
            'isImportant'      => $this->news->isImportant(),
            'validity'         => sprintf('%s - %s', $this->news->getValidityFrom()->format('d.m.Y'), $validityTo->format('d.m.Y')),
            'onlyValidityFrom' => $this->news->getValidityTo(true) === null,
            'tags'             => $tagsId,
        ];
    }
}
