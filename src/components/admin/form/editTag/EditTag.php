<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\News\BaseControl;
use Skadmin\News\Doctrine\News\NewsTag;
use Skadmin\News\Doctrine\News\NewsTagFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditTag extends FormWithUserControl
{
    use APackageControl;

    public LoaderFactory  $webLoader;
    private NewsTagFacade $facade;
    private NewsTag       $newsTag;

    public function __construct(?int $id, NewsTagFacade $facade, Translator $translator, LoggedUser $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->newsTag = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, BaseControl::PRIVILEGE_TAGS)) {
            $this->onFlashmessage('grid.news.overview-tag.name.flash.info.denide-acccess-tags', Flash::INFO);
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);
        }

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->newsTag->isLoaded()) {
            return new SimpleTranslation('news-tag.edit.title - %s', $this->newsTag->getName());
        }

        return 'news-tag.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('colorPicker'),
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('colorPicker'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->newsTag->isLoaded()) {
            $newsTag = $this->facade->update(
                $this->newsTag->getId(),
                $values->name,
                $values->content,
                $values->color,
                $values->isActive
            );
            $this->onFlashmessage('form.news-tag.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $newsTag = $this->facade->create(
                $values->name,
                $values->content,
                $values->color,
                $values->isActive
            );
            $this->onFlashmessage('form.news-tag.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-tag',
            'id'      => $newsTag->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-tag',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editTag.latte');

        $template->newsTag = $this->newsTag;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.news-tag.edit.name')
            ->setRequired('form.news-tag.edit.name.req');
        $form->addTextArea('content', 'form.news-tag.edit.content');
        $form->addText('color', 'form.news-tag.edit.color');

        $form->addCheckbox('isActive', 'form.news-tag.edit.is-active')
            ->setDefaultValue(true);

        // BUTTON
        $form->addSubmit('send', 'form.news-tag.edit.send');
        $form->addSubmit('sendBack', 'form.news-tag.edit.send-back');
        $form->addSubmit('back', 'form.news-tag.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->newsTag->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->newsTag->getName(),
            'content'  => $this->newsTag->getContent(),
            'color'    => $this->newsTag->getColor(),
            'isActive' => $this->newsTag->isActive(),
        ];
    }
}
