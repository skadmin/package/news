<?php

declare(strict_types=1);

namespace Skadmin\News\Components\Admin;

interface IEditTagFactory
{
    public function create(?int $id = null): EditTag;
}
