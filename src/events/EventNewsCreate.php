<?php

declare(strict_types=1);

namespace Skadmin\News\Events;

use Skadmin\News\Doctrine\News\News;
use Symfony\Contracts\EventDispatcher\Event;

class EventNewsCreate extends Event
{
    private News $news;

    public function __construct(News $news)
    {
        $this->news = $news;
    }

    public function getNews(): News
    {
        return $this->news;
    }
}
