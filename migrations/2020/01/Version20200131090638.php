<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131090638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'news.overview.title', 'hash' => 'c75878ac9ee393e5482595b0a3a54dad', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinky|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.action.new', 'hash' => '1b3b2ab69007488e300b6a53448804f4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit novinku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.name', 'hash' => 'bd72c98c59f3b1f3d48d7f7b2e50ea11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.is-active', 'hash' => '30cf4c96742b6f127cf72796e76746df', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.lastUpdateAuthor', 'hash' => '101771f7d4ab9e679c173df1754edb53', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.lastUpdateAt', 'hash' => '8b29b4b3c380aa5461185ccfec823a22', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední úprava', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.edit.title', 'hash' => 'bae6e34c4575ca43753a969f3625cf38', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.name', 'hash' => '482fa78810004a6a0ad4e499476c6412', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.name.req', 'hash' => '56700c967314ebbefdcbeaa3e1fa10b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.content', 'hash' => 'dfc8e4193b03d4dffa7cd3f862ed47e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.description', 'hash' => '429883e49b958c7d7d109fcd18782ff1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis - nebude-li vyplněn, vezme se část z obsahu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.is-active', 'hash' => 'ef36ffe5850b390e94d7b5a86f7599cc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.send', 'hash' => 'e58518c91d53d003f415552dceb6576b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.send-back', 'hash' => '39bdf21ae897dea733b90a29b8556220', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.back', 'hash' => '1b5041af64744a4410b308bb4bf9def5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.validity', 'hash' => 'eef0f012bc36210bf028b41c7ecdd1dc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Platnost od - do', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.validity.req', 'hash' => 'f927b128ff90c2501d2a834d9b6ce241', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím platnost od -do', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.flash.success.create', 'hash' => '8fa47465bd049a6460d9ed5306009940', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinka byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.detail.title - %s', 'hash' => '6c1a16a2812a5ac342997df33ee748a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Detail novinky - %s', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.detail.is-active', 'hash' => '1c083b1c35bb66f82befd80be8afc60c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.detail.validity', 'hash' => 'b7ded5d0edf6e4bcebad93233f07b144', 'module' => 'admin', 'language_id' => 1, 'singular' => 'platnost', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.detail.last-update-at', 'hash' => 'd418a4dbc010e4f9642ed2b0074f9903', 'module' => 'admin', 'language_id' => 1, 'singular' => 'poslední úprava', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.action.edit', 'hash' => 'edf11f8730bc5a6d0f46fa0132f8b676', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.action.detail', 'hash' => '625a283527c9a133a3feaad91e581e51', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.flash.success.update', 'hash' => '0e78cefd1a93cdc51630012f612224ba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinka byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.edit.title - %s', 'hash' => '48c21a71cd6bd784498fe08f49982977', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.news.title', 'hash' => 'f2bfcf8bfdc9dac39cfc3a23c70426b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.news.description', 'hash' => 'd37d67a9df89774a2900c81d889ec20c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje upravovat novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.overview', 'hash' => '58066cfc1b53b519daba9667c3eb8c2a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.image-preview', 'hash' => '83855fcb872a4ef9cf3edb71064f4272', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.image-preview.rule-image', 'hash' => '8c12477faea372b5306220ede54bdc1c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
