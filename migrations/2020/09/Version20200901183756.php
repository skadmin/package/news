<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901183756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'grid.news.overview.tags', 'hash' => '2b3ca249301deb7e598cad78fb92f191', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview.action.overview-tag', 'hash' => '331a76e2e1af90af77739a845ce6cc39', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled štítků', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.tags', 'hash' => '80c04e8a4537789e4a69fbb0ece3a49c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.tags.placeholder', 'hash' => '5774d707b536ec2df1de6a6f04a03301', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.action.overview', 'hash' => '352bfef2199948c3282c4bdefa05f6e5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled aktualit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.action.new', 'hash' => '710b4c9580187b2b14fbf05a93aaec2d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit štítek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.name', 'hash' => 'b0eeb506dea6d3c3b24ba0cdf091145b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.color', 'hash' => 'cba9f7d65c8c1a3685df56d71db90cb8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.action.edit', 'hash' => '0e57a17b7b7d4d6f2caf1b1cd073b88c', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news-tag.edit.title', 'hash' => 'a9bcaa559c9ebb47f7d4462fdf4726d4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení štítku novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.name', 'hash' => '2b2516eb5fbea552037d4c705869b93f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.name.req', 'hash' => 'e312ea8a17cd6f10795bd307f73e885c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.color', 'hash' => 'f0b68650abcca47d4f5a22a8973c86fd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Barva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.content', 'hash' => 'a6bb54f1c160d79e876e967a6a4738aa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.is-active', 'hash' => '2c740a4d8ccc5265df153a0faa685f80', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.send', 'hash' => '8e045aad4d398e77b6327eaebf51bb5c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.send-back', 'hash' => '080dec8065416f710c6f43377c412fbd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.back', 'hash' => 'd9357948666befc1477475cc574703b4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.flash.success.create', 'hash' => '354d9c70bb66c051bcf45fd3f3d8dd97', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek novinky byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news-tag.edit.title - %s', 'hash' => '52cb0def698eb29bf51cdcd933106c7d', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace štítku novinky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news-tag.edit.flash.success.update', 'hash' => '951c7f75f71e8aa70fc551b4d1d31d85', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítek novinky byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.news.overview-tag.action.flash.sort.success', 'hash' => 'fcd669f272d15aedb102c467cbfde248', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí štítků novinek bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'news.overview-tag.title', 'hash' => '1bb6d18db37e294b3bb82a2e463bcf67', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Štítky novinek|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.tags.no-result', 'hash' => 'b100b03cbcc8396104043dd5990c0d75', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyl nalezen žádný štítek: ', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
