<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200513070506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news ADD is_locked TINYINT(1) DEFAULT \'0\' NOT NULL');

        $translations = [
            ['original' => 'form.news.edit.only_validity_from', 'hash' => '43febf4c6b75ddc26865604bf3e7fe3a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'platnost do se nezohlední (novinka bude trvalá)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.is-locked', 'hash' => 'c10defe9a76a2ed68ad598b6746c0a8e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je zamčená', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.news.edit.flash.info.locked', 'hash' => 'd0cba5cde0f13f9221f35079296da7e6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Novinku nemůžete upravovat, jelikož byla uzamčena.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE news DROP is_locked');
    }
}
